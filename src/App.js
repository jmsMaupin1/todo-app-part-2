import React, { Component } from "react";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import { BrowserRouter as Router, Route, NavLink as Link } from 'react-router-dom';
import * as todosActions from './actions/todosActions';
import stateStore from './containers/StoreState';
import TodoList from './components/TodoList';
import "./index.css";

class App extends Component {
  
  deleteCompleteTodos = () => {
    this.props.todosActions.removeCompleted();
  }

  deleteTodo = todo => {
    this.props.todosActions.deleteTodo(todo.id);
  }

  toggleCompleted = todo => {    
    this.props.todosActions.toggleCompleted(todo.id)
  }

  createToDo = e => {
    if(e.key === "Enter") {
      let {value} = e.target;
      this.props.todosActions.addTodo({
        userId: 1,
        id: this.props.todos.length + 1,
        title: value,
        completed: false
      })

      e.target.value = "";
    }
  }

  componentDidMount() {
    let todos = JSON.parse(this.props.load('todos')) || [];

    this.props.todosActions.setTodos(todos);

    window.addEventListener('storage', () => {
      let todos = JSON.parse(this.props.load('todos')) || [];
      this.props.todosActions.setTodos(todos);
    })
  }

  componentWillUnmount() {
    window.removeEventListener('storage', () => {
      let todos = JSON.parse(this.props.load('todos')) || [];
      this.props.todosActions.setTodos(todos);
    })
  }
  
  

  render() {
    return (
      <Router basename="/todo-app-part-2">
        <section className="todoapp">
          <header className="header">
            <h1>todos</h1>
            <input
              className="new-todo"
              placeholder="What needs to be done?"
              onKeyDown={this.createToDo}
              autoFocus
            />
          </header>
              <Route exact path="/completed" render={() => {
                return (
                  <TodoList 
                    todos={this.props.todos}
                    filter={{completed: true}}
                    deleteTodo={this.deleteTodo}
                    toggleCompleted={this.toggleCompleted.bind(this)}
                  />)
                }}/>
              <Route exact path="/active" render = {() => {
                return (<TodoList 
                  todos={this.props.todos}
                  filter={{completed: false}}
                  deleteTodo={this.deleteTodo}
                  toggleCompleted={this.toggleCompleted.bind(this)}
                />)
              }} />
              <Route exact path="/" render = {() => {
                return (<TodoList 
                  todos={this.props.todos}
                  filter={{}}
                  deleteTodo={this.deleteTodo}
                  toggleCompleted={this.toggleCompleted.bind(this)}
                />)
              }} />
          <footer className="footer">
            <span className="todo-count">
              <strong>
                {
                  this.props.todos
                  .filter(todos => !todos.completed)
                  .length
                }
              </strong> item(s) left
            </span>
            <ul className="filters">
              <li>
                <Link activeClassName="selected" exact to="/">All</Link>
              </li>
              <li>
                <Link activeClassName="selected" exact to="/active">Active</Link>
              </li>
              <li>
                <Link activeClassName="selected" exact to="/completed">Completed</Link>
              </li>
            </ul>
            <button className="clear-completed" onClick={this.deleteCompleteTodos}>Clear completed</button>
          </footer>
        </section>
      </Router>
    );
  }
}

function mapStateToProps(state) {
  return {
    todos: state.todosReducer.todos
  }
}

function matchDispatchToProps(dispatch) {
  return {
    todosActions: bindActionCreators(todosActions, dispatch)
  }
}

export default stateStore("App")(connect(
  mapStateToProps,
  matchDispatchToProps
)(App))
