import React, { Component } from 'react'

export default class TodoItem extends Component {
    render() {
      return (
        <li className={this.props.completed ? "completed" : ""}>
          <div className="view">
            <input
              className="toggle"
              type="checkbox"
              checked={this.props.completed}
              onChange={this.props.toggleCompleted.bind(this, this.props)}
            />
            <label>{this.props.title}</label>
            <button className="destroy" onClick={this.props.deleteTodo.bind(this, this.props)}/>
          </div>
        </li>
      );
    }
  }