import * as types from './actionTypes';

export const addTodo = title => {
    return {
        type: types.ADD_TODO,
        title
    }
}

export const toggleCompleted = id => {
    return {
        type: types.TOGGLE_COMPLETED,
        id
    }
}

export const deleteTodo = id => {
    return {
        type: types.DELETE_TODO,
        id
    }
}

export const removeCompleted = () => {
    return {
        type: types.REMOVE_COMPLETED
    }
}

export const setTodos = todos => {
    return {
        type: types.SET_TODOS,
        todos
    }
}