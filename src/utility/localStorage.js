export const isLocalStorageAvailable = () => {
    const testKey = 'test';

    try {
        localStorage.setItem(testKey, testKey);
        localStorage.removeItem(testKey);
        return true;
    } catch(e) {
        return false
    }
}

export const save = (key, data) => {
    localStorage.setItem(key, data)
}
